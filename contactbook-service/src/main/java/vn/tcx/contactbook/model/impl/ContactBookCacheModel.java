/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package vn.tcx.contactbook.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import vn.tcx.contactbook.model.ContactBook;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing ContactBook in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see ContactBook
 * @generated
 */
@ProviderType
public class ContactBookCacheModel implements CacheModel<ContactBook>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ContactBookCacheModel)) {
			return false;
		}

		ContactBookCacheModel contactBookCacheModel = (ContactBookCacheModel)obj;

		if (contactId == contactBookCacheModel.contactId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, contactId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(25);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", contactId=");
		sb.append(contactId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", name=");
		sb.append(name);
		sb.append(", deleted=");
		sb.append(deleted);
		sb.append(", birthday=");
		sb.append(birthday);
		sb.append(", address=");
		sb.append(address);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ContactBook toEntityModel() {
		ContactBookImpl contactBookImpl = new ContactBookImpl();

		if (uuid == null) {
			contactBookImpl.setUuid(StringPool.BLANK);
		}
		else {
			contactBookImpl.setUuid(uuid);
		}

		contactBookImpl.setContactId(contactId);
		contactBookImpl.setGroupId(groupId);
		contactBookImpl.setCompanyId(companyId);
		contactBookImpl.setUserId(userId);

		if (userName == null) {
			contactBookImpl.setUserName(StringPool.BLANK);
		}
		else {
			contactBookImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			contactBookImpl.setCreateDate(null);
		}
		else {
			contactBookImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			contactBookImpl.setModifiedDate(null);
		}
		else {
			contactBookImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (name == null) {
			contactBookImpl.setName(StringPool.BLANK);
		}
		else {
			contactBookImpl.setName(name);
		}

		contactBookImpl.setDeleted(deleted);

		if (birthday == Long.MIN_VALUE) {
			contactBookImpl.setBirthday(null);
		}
		else {
			contactBookImpl.setBirthday(new Date(birthday));
		}

		if (address == null) {
			contactBookImpl.setAddress(StringPool.BLANK);
		}
		else {
			contactBookImpl.setAddress(address);
		}

		contactBookImpl.resetOriginalValues();

		return contactBookImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		contactId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		name = objectInput.readUTF();

		deleted = objectInput.readBoolean();
		birthday = objectInput.readLong();
		address = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(contactId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (name == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(name);
		}

		objectOutput.writeBoolean(deleted);
		objectOutput.writeLong(birthday);

		if (address == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(address);
		}
	}

	public String uuid;
	public long contactId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String name;
	public boolean deleted;
	public long birthday;
	public String address;
}