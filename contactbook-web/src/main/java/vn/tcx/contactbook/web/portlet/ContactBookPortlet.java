package vn.tcx.contactbook.web.portlet;

import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.DateFormatFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;

import aQute.bnd.annotation.metatype.Configurable;
import vn.tcx.contactbook.model.ContactBook;
import vn.tcx.contactbook.service.ContactBookLocalServiceUtil;
import vn.tcx.contactbook.service.persistence.ContactBookPersistence;
import vn.tcx.contactbook.service.persistence.ContactBookUtil;
import vn.tcx.contactbook.web.constants.ContactBookPortletKeys;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;

@Component(
	configurationPid = "vn.tcx.contactbook.web.portlet.ContactBookConfiguration",
	immediate = true,
	property = {
		"com.liferay.portlet.add-default-resource=true",
		"com.liferay.portlet.css-class-wrapper=portlet-chat",
		"com.liferay.portlet.icon=/icons/contact.png",
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=Contact Book",
		"javax.portlet.expiration-cache=0",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + ContactBookPortletKeys.CONTACTBOOK,
		"javax.portlet.portlet-info.keywords=Contact Book",
		"javax.portlet.portlet-info.short-title=Contact Book",
		"javax.portlet.portlet-info.title=Contact Book",
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class ContactBookPortlet extends MVCPortlet {
	
	
	@Override
    public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {

        renderRequest.setAttribute(ContactBookConfiguration.class.getName(), _configuration);

        super.doView(renderRequest, renderResponse);
    }

	
	public void addContact(ActionRequest actionRequest, ActionResponse actionResponse){
		String name = ParamUtil.getString(actionRequest, "name");
		String address = ParamUtil.getString(actionRequest, "address");
		Date birthday = ParamUtil.getDate(actionRequest, "birthday", DateFormatFactoryUtil.getSimpleDateFormat("dd/mm/yyyy")); 
		
		//long id = CounterLocalServiceUtil.increment(name);
		ContactBook contact = ContactBookUtil.create(0);
		contact.setName(name);
		contact.setBirthday(birthday);
		contact.setAddress(address);
		
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		contact.setCompanyId(themeDisplay.getCompanyId());
		contact.setGroupId(themeDisplay.getCompanyGroupId());
		contact.setUserName(themeDisplay.getUser().getScreenName());
		contact.setUuid(themeDisplay.getUser().getUuid());
		
		ContactBookLocalServiceUtil.addContactBook(contact);
		System.out.println("Done");
	}
	
	
	//Referencing configuration
	public String getFavoriteColor(Map<String, String> colors) {
       return colors.get(_configuration.favoriteColor());
    }

    @Activate
    @Modified
    protected void activate(Map<String, Object> properties) {
        _configuration = Configurable.createConfigurable(
        		ContactBookConfiguration.class, properties);
    }

    private volatile ContactBookConfiguration _configuration;
	
}