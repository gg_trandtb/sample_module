package vn.tcx.contactbook.web.portlet;

import java.util.Map;

import javax.jws.Oneway;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;

import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.util.ParamUtil;

import aQute.bnd.annotation.metatype.Configurable;
import vn.tcx.contactbook.web.constants.ContactBookPortletKeys;

@Component(
		configurationPid="vn.tcx.contactbook.web.portlet.ContactBookConfiguration",
		configurationPolicy = ConfigurationPolicy.OPTIONAL,
		immediate = true,
		property = {
		    "javax.portlet.name="+ ContactBookPortletKeys.CONTACTBOOK,
		},
		service= ConfigurationAction.class
	)
public class ContactBookConfigurationAction extends DefaultConfigurationAction {

	private volatile ContactBookConfiguration _configuration;
	
	@Activate
    @Modified
    protected void activate(Map<String, Object> properties) {
        _configuration = Configurable.createConfigurable(
        		ContactBookConfiguration.class, properties);
    }

	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse)
			throws Exception {
		
		String favoriteColor = ParamUtil.getString(actionRequest, "favoriteColor");
        setPreference(actionRequest, "favoriteColor", favoriteColor);
        
        String[] validLanguages = ParamUtil.getStringValues(actionRequest, "validLanguages");
        setPreference(actionRequest, "validLanguages", validLanguages);
        
        String itemsPerPage = ParamUtil.getString(actionRequest, "itemsPerPage");
        setPreference(actionRequest, "itemsPerPage", itemsPerPage);
        
		super.processAction(portletConfig, actionRequest, actionResponse);
	}
	
    @Override
	public void include(PortletConfig portletConfig, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
    	
    	request.setAttribute(ContactBookConfiguration.class.getName(),_configuration);
    	
		super.include(portletConfig, request, response);
	}

	//Referencing configuration
  	public String getFavoriteColor(Map<String, String> colors) {
    	return colors.get(_configuration.favoriteColor());
    }
  	
  	public String[] getValidLanguages(Map<String, String[]> langs){
  		return langs.get(_configuration.validLanguages());
  	}
	
	public int getItemsPerPage(Map<String, Integer> itemsPerPage){
		return itemsPerPage.get(_configuration.itemsPerPage());
	}
}
