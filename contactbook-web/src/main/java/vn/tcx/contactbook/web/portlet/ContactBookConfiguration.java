package vn.tcx.contactbook.web.portlet;

import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;

import aQute.bnd.annotation.metatype.Meta;

@ExtendedObjectClassDefinition(category = "Green Global")
@Meta.OCD(
		name="ContactBook Configuration", 
		id="vn.tcx.contactbook.web.portlet.ContactBookConfiguration"
		)
public interface ContactBookConfiguration {

	@Meta.AD(
	    deflt = "blue",
	    required = false
	)
	public String favoriteColor();
	
	@Meta.AD(
	   deflt = "red|green|blue|black",
	   required = false
	)
	public String[] validLanguages();
	
	@Meta.AD(required = false)
	public int itemsPerPage();

	    
}
