<%@page import="vn.tcx.contactbook.service.ContactBookLocalServiceUtil"%>
<%@page import="vn.tcx.contactbook.model.ContactBook"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%-- <%@ include file="/init.jsp" %> --%>

<portlet:actionURL name="addContact" var="addContactURL">
</portlet:actionURL>

<aui:form name="fm" action="<%=addContactURL %>" method="post">
	<aui:input name="name" type="text" label="Name" />
	<aui:input name="birthday" type="date" label="Birthday" />
	<aui:input name="address" type="textarea" label="Address" />
	<aui:button type="submit" />
</aui:form>