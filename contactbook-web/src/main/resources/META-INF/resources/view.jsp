<%@ include file="/init.jsp" %>

<portlet:renderURL var="editURL">
	<portlet:param name="mvcPath" value="/edit.jsp" />
</portlet:renderURL>

<p>
	<b><liferay-ui:message key="contactbook_web_ContactBook.caption"/></b>
</p>

<p>Favorite color: <span style="color: <%= favoriteColor %>;"><%= favoriteColor %></span></p>

<a href="<%=editURL %>">Add PPL</a>
<br />
<%
	List<ContactBook> contacts = ContactBookLocalServiceUtil.getContactBooks(-1, -1);
	if (contacts != null) {
		for (ContactBook ct : contacts) {
%>
			<%=ct.getName() %> - <%=ct.getBirthday() %> - <%=ct.getAddress() %>  <br />
<%
		}
	}
%>