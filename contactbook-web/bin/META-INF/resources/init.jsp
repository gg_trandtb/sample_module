<%@page import="com.liferay.portal.kernel.util.StringPool"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.kernel.util.GetterUtil"%>
<%@page import="vn.tcx.contactbook.web.portlet.ContactBookConfiguration"%>
<%@page import="vn.tcx.contactbook.service.ContactBookLocalServiceUtil"%>
<%@page import="vn.tcx.contactbook.model.ContactBook"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<liferay-theme:defineObjects />

<portlet:defineObjects />

<%
	ContactBookConfiguration configuration =
        (ContactBookConfiguration) renderRequest.getAttribute(ContactBookConfiguration.class.getName());

    String favoriteColor = StringPool.BLANK;
    String[] validLanguages = StringPool.EMPTY_ARRAY;
    String itemsPerPage = "10";

    if (Validator.isNotNull(configuration)) {
        favoriteColor =
            portletPreferences.getValue(
                "favoriteColor", configuration.favoriteColor());
        
        validLanguages =
                portletPreferences.getValues(
                    "validLanguages", configuration.validLanguages());
        
        itemsPerPage =
                portletPreferences.getValue(
                    "itemsPerPage", ""+configuration.itemsPerPage());
        
        
    }
%>