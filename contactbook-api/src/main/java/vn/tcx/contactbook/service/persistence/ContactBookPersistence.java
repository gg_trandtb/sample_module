/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package vn.tcx.contactbook.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import vn.tcx.contactbook.exception.NoSuchContactBookException;
import vn.tcx.contactbook.model.ContactBook;

/**
 * The persistence interface for the contact book service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see vn.tcx.contactbook.service.persistence.impl.ContactBookPersistenceImpl
 * @see ContactBookUtil
 * @generated
 */
@ProviderType
public interface ContactBookPersistence extends BasePersistence<ContactBook> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ContactBookUtil} to access the contact book persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the contact books where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching contact books
	*/
	public java.util.List<ContactBook> findByUuid(java.lang.String uuid);

	/**
	* Returns a range of all the contact books where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContactBookModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of contact books
	* @param end the upper bound of the range of contact books (not inclusive)
	* @return the range of matching contact books
	*/
	public java.util.List<ContactBook> findByUuid(java.lang.String uuid,
		int start, int end);

	/**
	* Returns an ordered range of all the contact books where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContactBookModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of contact books
	* @param end the upper bound of the range of contact books (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching contact books
	*/
	public java.util.List<ContactBook> findByUuid(java.lang.String uuid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ContactBook> orderByComparator);

	/**
	* Returns an ordered range of all the contact books where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContactBookModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of contact books
	* @param end the upper bound of the range of contact books (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching contact books
	*/
	public java.util.List<ContactBook> findByUuid(java.lang.String uuid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ContactBook> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first contact book in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching contact book
	* @throws NoSuchContactBookException if a matching contact book could not be found
	*/
	public ContactBook findByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ContactBook> orderByComparator)
		throws NoSuchContactBookException;

	/**
	* Returns the first contact book in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching contact book, or <code>null</code> if a matching contact book could not be found
	*/
	public ContactBook fetchByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ContactBook> orderByComparator);

	/**
	* Returns the last contact book in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching contact book
	* @throws NoSuchContactBookException if a matching contact book could not be found
	*/
	public ContactBook findByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ContactBook> orderByComparator)
		throws NoSuchContactBookException;

	/**
	* Returns the last contact book in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching contact book, or <code>null</code> if a matching contact book could not be found
	*/
	public ContactBook fetchByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ContactBook> orderByComparator);

	/**
	* Returns the contact books before and after the current contact book in the ordered set where uuid = &#63;.
	*
	* @param contactId the primary key of the current contact book
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next contact book
	* @throws NoSuchContactBookException if a contact book with the primary key could not be found
	*/
	public ContactBook[] findByUuid_PrevAndNext(long contactId,
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ContactBook> orderByComparator)
		throws NoSuchContactBookException;

	/**
	* Removes all the contact books where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public void removeByUuid(java.lang.String uuid);

	/**
	* Returns the number of contact books where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching contact books
	*/
	public int countByUuid(java.lang.String uuid);

	/**
	* Returns the contact book where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchContactBookException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching contact book
	* @throws NoSuchContactBookException if a matching contact book could not be found
	*/
	public ContactBook findByUUID_G(java.lang.String uuid, long groupId)
		throws NoSuchContactBookException;

	/**
	* Returns the contact book where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching contact book, or <code>null</code> if a matching contact book could not be found
	*/
	public ContactBook fetchByUUID_G(java.lang.String uuid, long groupId);

	/**
	* Returns the contact book where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching contact book, or <code>null</code> if a matching contact book could not be found
	*/
	public ContactBook fetchByUUID_G(java.lang.String uuid, long groupId,
		boolean retrieveFromCache);

	/**
	* Removes the contact book where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the contact book that was removed
	*/
	public ContactBook removeByUUID_G(java.lang.String uuid, long groupId)
		throws NoSuchContactBookException;

	/**
	* Returns the number of contact books where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching contact books
	*/
	public int countByUUID_G(java.lang.String uuid, long groupId);

	/**
	* Returns all the contact books where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching contact books
	*/
	public java.util.List<ContactBook> findByUuid_C(java.lang.String uuid,
		long companyId);

	/**
	* Returns a range of all the contact books where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContactBookModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of contact books
	* @param end the upper bound of the range of contact books (not inclusive)
	* @return the range of matching contact books
	*/
	public java.util.List<ContactBook> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end);

	/**
	* Returns an ordered range of all the contact books where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContactBookModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of contact books
	* @param end the upper bound of the range of contact books (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching contact books
	*/
	public java.util.List<ContactBook> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ContactBook> orderByComparator);

	/**
	* Returns an ordered range of all the contact books where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContactBookModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of contact books
	* @param end the upper bound of the range of contact books (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching contact books
	*/
	public java.util.List<ContactBook> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ContactBook> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first contact book in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching contact book
	* @throws NoSuchContactBookException if a matching contact book could not be found
	*/
	public ContactBook findByUuid_C_First(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ContactBook> orderByComparator)
		throws NoSuchContactBookException;

	/**
	* Returns the first contact book in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching contact book, or <code>null</code> if a matching contact book could not be found
	*/
	public ContactBook fetchByUuid_C_First(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ContactBook> orderByComparator);

	/**
	* Returns the last contact book in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching contact book
	* @throws NoSuchContactBookException if a matching contact book could not be found
	*/
	public ContactBook findByUuid_C_Last(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ContactBook> orderByComparator)
		throws NoSuchContactBookException;

	/**
	* Returns the last contact book in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching contact book, or <code>null</code> if a matching contact book could not be found
	*/
	public ContactBook fetchByUuid_C_Last(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ContactBook> orderByComparator);

	/**
	* Returns the contact books before and after the current contact book in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param contactId the primary key of the current contact book
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next contact book
	* @throws NoSuchContactBookException if a contact book with the primary key could not be found
	*/
	public ContactBook[] findByUuid_C_PrevAndNext(long contactId,
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ContactBook> orderByComparator)
		throws NoSuchContactBookException;

	/**
	* Removes all the contact books where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public void removeByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns the number of contact books where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching contact books
	*/
	public int countByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns all the contact books where name = &#63;.
	*
	* @param name the name
	* @return the matching contact books
	*/
	public java.util.List<ContactBook> findByName(java.lang.String name);

	/**
	* Returns a range of all the contact books where name = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContactBookModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param name the name
	* @param start the lower bound of the range of contact books
	* @param end the upper bound of the range of contact books (not inclusive)
	* @return the range of matching contact books
	*/
	public java.util.List<ContactBook> findByName(java.lang.String name,
		int start, int end);

	/**
	* Returns an ordered range of all the contact books where name = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContactBookModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param name the name
	* @param start the lower bound of the range of contact books
	* @param end the upper bound of the range of contact books (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching contact books
	*/
	public java.util.List<ContactBook> findByName(java.lang.String name,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ContactBook> orderByComparator);

	/**
	* Returns an ordered range of all the contact books where name = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContactBookModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param name the name
	* @param start the lower bound of the range of contact books
	* @param end the upper bound of the range of contact books (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching contact books
	*/
	public java.util.List<ContactBook> findByName(java.lang.String name,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ContactBook> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first contact book in the ordered set where name = &#63;.
	*
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching contact book
	* @throws NoSuchContactBookException if a matching contact book could not be found
	*/
	public ContactBook findByName_First(java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator<ContactBook> orderByComparator)
		throws NoSuchContactBookException;

	/**
	* Returns the first contact book in the ordered set where name = &#63;.
	*
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching contact book, or <code>null</code> if a matching contact book could not be found
	*/
	public ContactBook fetchByName_First(java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator<ContactBook> orderByComparator);

	/**
	* Returns the last contact book in the ordered set where name = &#63;.
	*
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching contact book
	* @throws NoSuchContactBookException if a matching contact book could not be found
	*/
	public ContactBook findByName_Last(java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator<ContactBook> orderByComparator)
		throws NoSuchContactBookException;

	/**
	* Returns the last contact book in the ordered set where name = &#63;.
	*
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching contact book, or <code>null</code> if a matching contact book could not be found
	*/
	public ContactBook fetchByName_Last(java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator<ContactBook> orderByComparator);

	/**
	* Returns the contact books before and after the current contact book in the ordered set where name = &#63;.
	*
	* @param contactId the primary key of the current contact book
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next contact book
	* @throws NoSuchContactBookException if a contact book with the primary key could not be found
	*/
	public ContactBook[] findByName_PrevAndNext(long contactId,
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator<ContactBook> orderByComparator)
		throws NoSuchContactBookException;

	/**
	* Removes all the contact books where name = &#63; from the database.
	*
	* @param name the name
	*/
	public void removeByName(java.lang.String name);

	/**
	* Returns the number of contact books where name = &#63;.
	*
	* @param name the name
	* @return the number of matching contact books
	*/
	public int countByName(java.lang.String name);

	/**
	* Caches the contact book in the entity cache if it is enabled.
	*
	* @param contactBook the contact book
	*/
	public void cacheResult(ContactBook contactBook);

	/**
	* Caches the contact books in the entity cache if it is enabled.
	*
	* @param contactBooks the contact books
	*/
	public void cacheResult(java.util.List<ContactBook> contactBooks);

	/**
	* Creates a new contact book with the primary key. Does not add the contact book to the database.
	*
	* @param contactId the primary key for the new contact book
	* @return the new contact book
	*/
	public ContactBook create(long contactId);

	/**
	* Removes the contact book with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param contactId the primary key of the contact book
	* @return the contact book that was removed
	* @throws NoSuchContactBookException if a contact book with the primary key could not be found
	*/
	public ContactBook remove(long contactId) throws NoSuchContactBookException;

	public ContactBook updateImpl(ContactBook contactBook);

	/**
	* Returns the contact book with the primary key or throws a {@link NoSuchContactBookException} if it could not be found.
	*
	* @param contactId the primary key of the contact book
	* @return the contact book
	* @throws NoSuchContactBookException if a contact book with the primary key could not be found
	*/
	public ContactBook findByPrimaryKey(long contactId)
		throws NoSuchContactBookException;

	/**
	* Returns the contact book with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param contactId the primary key of the contact book
	* @return the contact book, or <code>null</code> if a contact book with the primary key could not be found
	*/
	public ContactBook fetchByPrimaryKey(long contactId);

	@Override
	public java.util.Map<java.io.Serializable, ContactBook> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the contact books.
	*
	* @return the contact books
	*/
	public java.util.List<ContactBook> findAll();

	/**
	* Returns a range of all the contact books.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContactBookModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of contact books
	* @param end the upper bound of the range of contact books (not inclusive)
	* @return the range of contact books
	*/
	public java.util.List<ContactBook> findAll(int start, int end);

	/**
	* Returns an ordered range of all the contact books.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContactBookModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of contact books
	* @param end the upper bound of the range of contact books (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of contact books
	*/
	public java.util.List<ContactBook> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ContactBook> orderByComparator);

	/**
	* Returns an ordered range of all the contact books.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ContactBookModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of contact books
	* @param end the upper bound of the range of contact books (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of contact books
	*/
	public java.util.List<ContactBook> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ContactBook> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the contact books from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of contact books.
	*
	* @return the number of contact books
	*/
	public int countAll();

	@Override
	public java.util.Set<java.lang.String> getBadColumnNames();
}