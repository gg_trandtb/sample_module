/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package vn.tcx.contactbook.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for ContactBook. This utility wraps
 * {@link vn.tcx.contactbook.service.impl.ContactBookLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see ContactBookLocalService
 * @see vn.tcx.contactbook.service.base.ContactBookLocalServiceBaseImpl
 * @see vn.tcx.contactbook.service.impl.ContactBookLocalServiceImpl
 * @generated
 */
@ProviderType
public class ContactBookLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link vn.tcx.contactbook.service.impl.ContactBookLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */
	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery getExportActionableDynamicQuery(
		com.liferay.exportimport.kernel.lar.PortletDataContext portletDataContext) {
		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of contact books.
	*
	* @return the number of contact books
	*/
	public static int getContactBooksCount() {
		return getService().getContactBooksCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link vn.tcx.contactbook.model.impl.ContactBookModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link vn.tcx.contactbook.model.impl.ContactBookModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns a range of all the contact books.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link vn.tcx.contactbook.model.impl.ContactBookModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of contact books
	* @param end the upper bound of the range of contact books (not inclusive)
	* @return the range of contact books
	*/
	public static java.util.List<vn.tcx.contactbook.model.ContactBook> getContactBooks(
		int start, int end) {
		return getService().getContactBooks(start, end);
	}

	/**
	* Returns all the contact books matching the UUID and company.
	*
	* @param uuid the UUID of the contact books
	* @param companyId the primary key of the company
	* @return the matching contact books, or an empty list if no matches were found
	*/
	public static java.util.List<vn.tcx.contactbook.model.ContactBook> getContactBooksByUuidAndCompanyId(
		java.lang.String uuid, long companyId) {
		return getService().getContactBooksByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Returns a range of contact books matching the UUID and company.
	*
	* @param uuid the UUID of the contact books
	* @param companyId the primary key of the company
	* @param start the lower bound of the range of contact books
	* @param end the upper bound of the range of contact books (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the range of matching contact books, or an empty list if no matches were found
	*/
	public static java.util.List<vn.tcx.contactbook.model.ContactBook> getContactBooksByUuidAndCompanyId(
		java.lang.String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<vn.tcx.contactbook.model.ContactBook> orderByComparator) {
		return getService()
				   .getContactBooksByUuidAndCompanyId(uuid, companyId, start,
			end, orderByComparator);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	/**
	* Adds the contact book to the database. Also notifies the appropriate model listeners.
	*
	* @param contactBook the contact book
	* @return the contact book that was added
	*/
	public static vn.tcx.contactbook.model.ContactBook addContactBook(
		vn.tcx.contactbook.model.ContactBook contactBook) {
		return getService().addContactBook(contactBook);
	}

	/**
	* Creates a new contact book with the primary key. Does not add the contact book to the database.
	*
	* @param contactId the primary key for the new contact book
	* @return the new contact book
	*/
	public static vn.tcx.contactbook.model.ContactBook createContactBook(
		long contactId) {
		return getService().createContactBook(contactId);
	}

	/**
	* Deletes the contact book with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param contactId the primary key of the contact book
	* @return the contact book that was removed
	* @throws PortalException if a contact book with the primary key could not be found
	*/
	public static vn.tcx.contactbook.model.ContactBook deleteContactBook(
		long contactId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteContactBook(contactId);
	}

	/**
	* Deletes the contact book from the database. Also notifies the appropriate model listeners.
	*
	* @param contactBook the contact book
	* @return the contact book that was removed
	*/
	public static vn.tcx.contactbook.model.ContactBook deleteContactBook(
		vn.tcx.contactbook.model.ContactBook contactBook) {
		return getService().deleteContactBook(contactBook);
	}

	public static vn.tcx.contactbook.model.ContactBook fetchContactBook(
		long contactId) {
		return getService().fetchContactBook(contactId);
	}

	/**
	* Returns the contact book matching the UUID and group.
	*
	* @param uuid the contact book's UUID
	* @param groupId the primary key of the group
	* @return the matching contact book, or <code>null</code> if a matching contact book could not be found
	*/
	public static vn.tcx.contactbook.model.ContactBook fetchContactBookByUuidAndGroupId(
		java.lang.String uuid, long groupId) {
		return getService().fetchContactBookByUuidAndGroupId(uuid, groupId);
	}

	/**
	* Returns the contact book with the primary key.
	*
	* @param contactId the primary key of the contact book
	* @return the contact book
	* @throws PortalException if a contact book with the primary key could not be found
	*/
	public static vn.tcx.contactbook.model.ContactBook getContactBook(
		long contactId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getContactBook(contactId);
	}

	/**
	* Returns the contact book matching the UUID and group.
	*
	* @param uuid the contact book's UUID
	* @param groupId the primary key of the group
	* @return the matching contact book
	* @throws PortalException if a matching contact book could not be found
	*/
	public static vn.tcx.contactbook.model.ContactBook getContactBookByUuidAndGroupId(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getContactBookByUuidAndGroupId(uuid, groupId);
	}

	/**
	* Updates the contact book in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param contactBook the contact book
	* @return the contact book that was updated
	*/
	public static vn.tcx.contactbook.model.ContactBook updateContactBook(
		vn.tcx.contactbook.model.ContactBook contactBook) {
		return getService().updateContactBook(contactBook);
	}

	public static ContactBookLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ContactBookLocalService, ContactBookLocalService> _serviceTracker =
		ServiceTrackerFactory.open(ContactBookLocalService.class);
}