/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package vn.tcx.contactbook.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.exportimport.kernel.lar.StagedModelType;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link ContactBook}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ContactBook
 * @generated
 */
@ProviderType
public class ContactBookWrapper implements ContactBook,
	ModelWrapper<ContactBook> {
	public ContactBookWrapper(ContactBook contactBook) {
		_contactBook = contactBook;
	}

	@Override
	public Class<?> getModelClass() {
		return ContactBook.class;
	}

	@Override
	public String getModelClassName() {
		return ContactBook.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("contactId", getContactId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("name", getName());
		attributes.put("deleted", getDeleted());
		attributes.put("birthday", getBirthday());
		attributes.put("address", getAddress());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long contactId = (Long)attributes.get("contactId");

		if (contactId != null) {
			setContactId(contactId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		Boolean deleted = (Boolean)attributes.get("deleted");

		if (deleted != null) {
			setDeleted(deleted);
		}

		Date birthday = (Date)attributes.get("birthday");

		if (birthday != null) {
			setBirthday(birthday);
		}

		String address = (String)attributes.get("address");

		if (address != null) {
			setAddress(address);
		}
	}

	/**
	* Returns the deleted of this contact book.
	*
	* @return the deleted of this contact book
	*/
	@Override
	public boolean getDeleted() {
		return _contactBook.getDeleted();
	}

	@Override
	public boolean isCachedModel() {
		return _contactBook.isCachedModel();
	}

	/**
	* Returns <code>true</code> if this contact book is deleted.
	*
	* @return <code>true</code> if this contact book is deleted; <code>false</code> otherwise
	*/
	@Override
	public boolean isDeleted() {
		return _contactBook.isDeleted();
	}

	@Override
	public boolean isEscapedModel() {
		return _contactBook.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _contactBook.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _contactBook.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<vn.tcx.contactbook.model.ContactBook> toCacheModel() {
		return _contactBook.toCacheModel();
	}

	@Override
	public int compareTo(vn.tcx.contactbook.model.ContactBook contactBook) {
		return _contactBook.compareTo(contactBook);
	}

	@Override
	public int hashCode() {
		return _contactBook.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _contactBook.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new ContactBookWrapper((ContactBook)_contactBook.clone());
	}

	/**
	* Returns the address of this contact book.
	*
	* @return the address of this contact book
	*/
	@Override
	public java.lang.String getAddress() {
		return _contactBook.getAddress();
	}

	/**
	* Returns the name of this contact book.
	*
	* @return the name of this contact book
	*/
	@Override
	public java.lang.String getName() {
		return _contactBook.getName();
	}

	/**
	* Returns the user name of this contact book.
	*
	* @return the user name of this contact book
	*/
	@Override
	public java.lang.String getUserName() {
		return _contactBook.getUserName();
	}

	/**
	* Returns the user uuid of this contact book.
	*
	* @return the user uuid of this contact book
	*/
	@Override
	public java.lang.String getUserUuid() {
		return _contactBook.getUserUuid();
	}

	/**
	* Returns the uuid of this contact book.
	*
	* @return the uuid of this contact book
	*/
	@Override
	public java.lang.String getUuid() {
		return _contactBook.getUuid();
	}

	@Override
	public java.lang.String toString() {
		return _contactBook.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _contactBook.toXmlString();
	}

	/**
	* Returns the birthday of this contact book.
	*
	* @return the birthday of this contact book
	*/
	@Override
	public Date getBirthday() {
		return _contactBook.getBirthday();
	}

	/**
	* Returns the create date of this contact book.
	*
	* @return the create date of this contact book
	*/
	@Override
	public Date getCreateDate() {
		return _contactBook.getCreateDate();
	}

	/**
	* Returns the modified date of this contact book.
	*
	* @return the modified date of this contact book
	*/
	@Override
	public Date getModifiedDate() {
		return _contactBook.getModifiedDate();
	}

	/**
	* Returns the company ID of this contact book.
	*
	* @return the company ID of this contact book
	*/
	@Override
	public long getCompanyId() {
		return _contactBook.getCompanyId();
	}

	/**
	* Returns the contact ID of this contact book.
	*
	* @return the contact ID of this contact book
	*/
	@Override
	public long getContactId() {
		return _contactBook.getContactId();
	}

	/**
	* Returns the group ID of this contact book.
	*
	* @return the group ID of this contact book
	*/
	@Override
	public long getGroupId() {
		return _contactBook.getGroupId();
	}

	/**
	* Returns the primary key of this contact book.
	*
	* @return the primary key of this contact book
	*/
	@Override
	public long getPrimaryKey() {
		return _contactBook.getPrimaryKey();
	}

	/**
	* Returns the user ID of this contact book.
	*
	* @return the user ID of this contact book
	*/
	@Override
	public long getUserId() {
		return _contactBook.getUserId();
	}

	@Override
	public vn.tcx.contactbook.model.ContactBook toEscapedModel() {
		return new ContactBookWrapper(_contactBook.toEscapedModel());
	}

	@Override
	public vn.tcx.contactbook.model.ContactBook toUnescapedModel() {
		return new ContactBookWrapper(_contactBook.toUnescapedModel());
	}

	@Override
	public void persist() {
		_contactBook.persist();
	}

	/**
	* Sets the address of this contact book.
	*
	* @param address the address of this contact book
	*/
	@Override
	public void setAddress(java.lang.String address) {
		_contactBook.setAddress(address);
	}

	/**
	* Sets the birthday of this contact book.
	*
	* @param birthday the birthday of this contact book
	*/
	@Override
	public void setBirthday(Date birthday) {
		_contactBook.setBirthday(birthday);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_contactBook.setCachedModel(cachedModel);
	}

	/**
	* Sets the company ID of this contact book.
	*
	* @param companyId the company ID of this contact book
	*/
	@Override
	public void setCompanyId(long companyId) {
		_contactBook.setCompanyId(companyId);
	}

	/**
	* Sets the contact ID of this contact book.
	*
	* @param contactId the contact ID of this contact book
	*/
	@Override
	public void setContactId(long contactId) {
		_contactBook.setContactId(contactId);
	}

	/**
	* Sets the create date of this contact book.
	*
	* @param createDate the create date of this contact book
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_contactBook.setCreateDate(createDate);
	}

	/**
	* Sets whether this contact book is deleted.
	*
	* @param deleted the deleted of this contact book
	*/
	@Override
	public void setDeleted(boolean deleted) {
		_contactBook.setDeleted(deleted);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_contactBook.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_contactBook.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_contactBook.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the group ID of this contact book.
	*
	* @param groupId the group ID of this contact book
	*/
	@Override
	public void setGroupId(long groupId) {
		_contactBook.setGroupId(groupId);
	}

	/**
	* Sets the modified date of this contact book.
	*
	* @param modifiedDate the modified date of this contact book
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_contactBook.setModifiedDate(modifiedDate);
	}

	/**
	* Sets the name of this contact book.
	*
	* @param name the name of this contact book
	*/
	@Override
	public void setName(java.lang.String name) {
		_contactBook.setName(name);
	}

	@Override
	public void setNew(boolean n) {
		_contactBook.setNew(n);
	}

	/**
	* Sets the primary key of this contact book.
	*
	* @param primaryKey the primary key of this contact book
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_contactBook.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_contactBook.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the user ID of this contact book.
	*
	* @param userId the user ID of this contact book
	*/
	@Override
	public void setUserId(long userId) {
		_contactBook.setUserId(userId);
	}

	/**
	* Sets the user name of this contact book.
	*
	* @param userName the user name of this contact book
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_contactBook.setUserName(userName);
	}

	/**
	* Sets the user uuid of this contact book.
	*
	* @param userUuid the user uuid of this contact book
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_contactBook.setUserUuid(userUuid);
	}

	/**
	* Sets the uuid of this contact book.
	*
	* @param uuid the uuid of this contact book
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_contactBook.setUuid(uuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ContactBookWrapper)) {
			return false;
		}

		ContactBookWrapper contactBookWrapper = (ContactBookWrapper)obj;

		if (Objects.equals(_contactBook, contactBookWrapper._contactBook)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _contactBook.getStagedModelType();
	}

	@Override
	public ContactBook getWrappedModel() {
		return _contactBook;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _contactBook.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _contactBook.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_contactBook.resetOriginalValues();
	}

	private final ContactBook _contactBook;
}